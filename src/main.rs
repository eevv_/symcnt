use std::collections::HashMap;

fn handle_arg(symbols: &mut HashMap<char, usize>, arg: String) -> Result<(), &'static str> {
    let contents = std::fs::read_to_string(arg)
        .map_err(|_| "cannot open specified file")?;
    
    let chars = contents
        .chars();

    // increment each char
    // counter in the map
    for char_ in chars {
        let counter = symbols.entry(char_).or_insert(0);
        *counter += 1;
    }

    Ok(())
}

fn main() {
    let mut args = std::env::args();
    args.next().unwrap();

    // hashmap of char -> usize
    let mut symbols = HashMap::new();

    for arg in args {
        let handle_result = handle_arg(&mut symbols, arg);
        if let Err(err) = handle_result {
            println!("{}", err);
            return;
        }
    }

    let mut symbols: Vec<(char, usize)> = symbols
        .iter()
        .map(|(a, b)| (*a, *b))
        .collect();
    
    // get the total number of symbols
    let total = symbols.iter().fold(0, |acc, (_, count)| acc + count) as f64;

    symbols.sort_by(|l, r| l.1.cmp(&r.1).reverse());

    for (char_, count) in &symbols {
        let count = *count;
        println!("{:?} {} {}", *char_, count, (count as f64) / total);
    }
}
